"use strict";
exports.__esModule = true;
var TransactionPdfType_1 = require("./TransactionPdfType");
var generateItemsTable_1 = require("./generateItemsTable");
exports.generateDocumentDef = function (params, fontDescriptors) {
    if (params === void 0) { params = {}; }
    if (fontDescriptors === void 0) { fontDescriptors = null; }
    var now = new Date();
    var transactionPdfType = params.transactionPdfType || TransactionPdfType_1.TransactionPdfType.Transaction;
    var defaultStyle = { lineHeight: 1.333 };
    if (fontDescriptors) {
        defaultStyle.font = Object.keys(fontDescriptors)[0];
    }
    var docDefinition = { content: [], styles: null, defaultStyle: defaultStyle };
    if (params.logo) {
        docDefinition.content.push({
            image: params.logo.image,
            fit: params.logo.fit,
            style: 'companyIcon'
        });
    }
    if (params.companyDetails) {
        docDefinition.content.push({
            text: params.companyDetails,
            style: 'companySubtext',
            margin: [0, 4, 0, 0]
        });
    }
    if (params.customerAddress) {
        docDefinition.content.push({
            text: params.customerAddress,
            margin: [0, 15, 0, 0]
        });
    }
    if (params.transactionId) {
        docDefinition.content.push({
            text: TransactionPdfType_1.TransactionPdfType[transactionPdfType] + " ID: " + params.transactionId,
            margin: [0, 10, 0, 0],
            style: 'rightAlign'
        });
    }
    docDefinition.content.push({
        text: params.transactionDate || now.toLocaleDateString(),
        style: 'rightAlign'
    });
    if (params.customerTaxId) {
        docDefinition.content.push({
            text: ((params.customerTaxIdType || '') + " " + params.customerTaxId).trim(),
            style: 'rightAlign'
        });
    }
    if (params.transactionPdfType === TransactionPdfType_1.TransactionPdfType.Receipt) {
        var paidText = params.paidDate ?
            "This receipt was paid " + params.paidDate + "." :
            "This receipt has been paid.";
        docDefinition.content.push({
            text: paidText,
            margin: [0, 5, 0, 0],
            style: "notification"
        });
    }
    docDefinition.content.push({
        margin: [0, 10, 0, 0],
        text: TransactionPdfType_1.TransactionPdfType[params.transactionPdfType],
        style: 'bold'
    });
    var itemsTable = generateItemsTable_1.generateItemsTable(params.items || [], now.toLocaleDateString(), params.taxRate || 0, params.taxType || '', 2, params.currency || '');
    docDefinition.content.push(itemsTable);
    (params.paymentMethods || []).forEach(function (method) {
        docDefinition.content.push({
            margin: [0, 10, 0, 0],
            text: method.name,
            style: 'bold'
        });
        docDefinition.content.push({
            text: method.details
        });
    });
    if (params.supportText) {
        docDefinition.content.push({
            margin: [0, 20, 0, 0],
            text: params.supportText
        });
    }
    if (params.thankYouText) {
        docDefinition.content.push({
            margin: [0, 5, 0, 0],
            text: params.thankYouText
        });
    }
    if (params.footerText) {
        docDefinition.content.push({
            margin: [0, 10, 0, 0],
            style: 'footerText',
            text: params.footerText
        });
    }
    docDefinition.styles = {
        companyName: {
            fontSize: 28,
            bold: true,
            color: '#30373a'
        },
        companyIcon: {
            marginBottom: 4
        },
        companySubtext: {
            fontSize: 8
        },
        centerAlign: {
            alignment: 'center'
        },
        rightAlign: {
            alignment: 'right'
        },
        notification: {
            bold: true,
            color: 'green'
        },
        bold: {
            bold: true
        },
        footerText: {
            fontSize: 10
        }
    };
    return docDefinition;
};
