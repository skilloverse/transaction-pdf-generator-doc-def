"use strict";
exports.__esModule = true;
var TransactionPdfType;
(function (TransactionPdfType) {
    TransactionPdfType[TransactionPdfType["Transaction"] = 0] = "Transaction";
    TransactionPdfType[TransactionPdfType["Receipt"] = 1] = "Receipt";
})(TransactionPdfType = exports.TransactionPdfType || (exports.TransactionPdfType = {}));
