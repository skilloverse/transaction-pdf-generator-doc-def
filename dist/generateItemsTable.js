"use strict";
exports.__esModule = true;
var TableCellDef = /** @class */ (function () {
    function TableCellDef() {
    }
    return TableCellDef;
}());
var TableDef = /** @class */ (function () {
    function TableDef() {
    }
    return TableDef;
}());
exports.generateItemsTable = function (items, defaultDateString, taxRate, taxType, decimalPlaces, currency) {
    var totalGrossPrice = 0;
    var heights = ['auto'];
    var i = 0;
    while (i++ < (items.length - 1)) {
        heights.push('auto');
    }
    heights.push(40, 'auto', 'auto', 'auto');
    var tableDef = {
        margin: [0, 10, 0, 0],
        layout: {
            defaultBorder: false
        },
        table: {
            headerRows: 1,
            widths: ['auto', '*', 'auto', 'auto'],
            heights: heights,
            body: [
                [
                    {
                        border: [false, false, false, true],
                        text: 'Quantity',
                        style: 'bold'
                    },
                    {
                        border: [false, false, false, true],
                        text: 'Description',
                        style: 'bold'
                    },
                    {
                        border: [false, false, false, true],
                        text: 'Date',
                        style: 'bold'
                    },
                    {
                        border: [false, false, false, true],
                        text: 'Amount',
                        style: ['bold', 'rightAlign']
                    },
                ],
            ]
        }
    };
    items.forEach(function (item, index) {
        var isFirst = index === 0;
        var isLast = index === (items.length - 1);
        totalGrossPrice += (item.unitPrice * item.quantity);
        tableDef.table.body.push([
            {
                text: item.quantity.toString(),
                border: [false, false, false, isLast],
                margin: isFirst ? [0, 5, 0, 0] : null,
                style: 'centerAlign'
            },
            {
                text: item.description,
                border: [false, false, false, isLast],
                margin: isFirst ? [0, 5, 0, 0] : null
            },
            {
                text: item.date || defaultDateString,
                border: [false, false, false, isLast],
                margin: isFirst ? [0, 5, 0, 0] : null
            },
            {
                text: ((item.unitPrice * item.quantity).toFixed(decimalPlaces) + " " + currency).trim(),
                border: [false, false, false, isLast],
                margin: isFirst ? [0, 5, 0, 0] : null,
                style: 'rightAlign'
            },
        ]);
    });
    var totalNetPrice = totalGrossPrice * (1 + taxRate);
    var totalTax = totalGrossPrice * taxRate;
    tableDef.table.body.push([
        {
            text: '',
            border: [false, false, false, false],
            margin: [0, 5, 0, 0]
        },
        {
            text: '',
            border: [false, false, false, false],
            margin: [0, 5, 0, 0]
        },
        {
            text: 'Sub total',
            border: [false, false, false, false],
            margin: [0, 5, 0, 0]
        },
        {
            text: (totalGrossPrice.toFixed(2) + " " + currency).trim(),
            border: [false, false, false, false],
            margin: [0, 5, 0, 0],
            style: 'rightAlign'
        }
    ]);
    tableDef.table.body.push([
        {
            text: '',
            border: [false, false, false, false]
        },
        {
            text: '',
            border: [false, false, false, false]
        },
        {
            text: taxType + " (" + taxRate * 100 + "%)",
            border: [false, false, false, false]
        },
        {
            text: (totalTax.toFixed(decimalPlaces) + " " + currency).trim(),
            border: [false, false, false, false],
            style: 'rightAlign'
        }
    ]);
    tableDef.table.body.push([
        {
            text: '',
            border: [false, false, false, false]
        },
        {
            text: '',
            border: [false, false, false, false]
        },
        {
            text: "Total",
            border: [false, false, false, false],
            style: 'bold'
        },
        {
            text: (totalNetPrice.toFixed(decimalPlaces) + " " + currency).trim(),
            style: 'rightAlign',
            border: [false, false, false, false]
        }
    ]);
    return tableDef;
};
