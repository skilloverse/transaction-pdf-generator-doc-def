import { TransactionPdfType } from './TransactionPdfType'

export class GenerateTransactionPdfParams {
    fontDescriptors?: any;
    logo?: {
        image: string;
        fit: number[];
    };
    companyDetails?: string;
    customerAddress?: string;
    customerTaxIdType?: string;
    customerTaxId?: string;
    transactionId?: string;
    transactionDate?: string;
    transactionPdfType?: TransactionPdfType;
    paidDate?: string;
    taxRate?: number;
    taxType?: string;
    items?: Array<{
        quantity: number;
        description: string;
        date: string;
        unitPrice: number;
    }>
    currency?: string;
    decimalPlaces?: number;
    paymentMethods?: Array<{
        name: string;
        details: string;
    }>;
    supportText?: string | any[];
    thankYouText?: string | any[];
    footerText?: string | any[];
}