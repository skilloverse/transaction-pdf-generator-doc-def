class TableCellDef {
    border: boolean[];
    text: string;
    style?: string | string[];
    margin?: number[];
}

class TableDef {
    margin: number[];
    layout: any;
    table: {
        headerRows: number;
        widths: Array<string | number>;
        heights: Array<string | number>;
        body: Array<Array<TableCellDef>>;
    };
}

export const generateItemsTable = (items: Array<{
    quantity: number;
    description: string;
    date: string;
    unitPrice: number;
}>,
defaultDateString: string,
taxRate: number,
taxType: string,
decimalPlaces: number,
currency: string,
) => {
    let totalGrossPrice = 0
    const heights: Array<string | number> = ['auto']
    let i = 0
    while (i++ < (items.length - 1)) {
        heights.push('auto')
    }
    heights.push(40, 'auto', 'auto', 'auto')
    const tableDef: TableDef = {
        margin: [0, 10, 0, 0],
        layout: {
            defaultBorder: false,
        },
        table: {
            headerRows: 1,
            widths: ['auto', '*', 'auto', 'auto'],
            heights,
            body: [
                [
                    {
                        border: [false, false, false, true],
                        text: 'Quantity',
                        style: 'bold',
                    },
                    {
                        border: [false, false, false, true],
                        text: 'Description',
                        style: 'bold',
                    },
                    {
                        border: [false, false, false, true],
                        text: 'Date',
                        style: 'bold',
                    },
                    {
                        border: [false, false, false, true],
                        text: 'Amount',
                        style: ['bold', 'rightAlign'],
                    },
                ],
            ],
        }
    }
    items.forEach((item, index) => {
        const isFirst = index === 0
        const isLast = index === (items.length - 1)
        totalGrossPrice += (item.unitPrice * item.quantity);
        tableDef.table.body.push([
            {
                text: item.quantity.toString(),
                border: [false, false, false, isLast],
                margin: isFirst ? [0, 5, 0, 0] : null,
                style: 'centerAlign',
            },
            {
                text: item.description,
                border: [false, false, false, isLast],
                margin: isFirst ? [0, 5, 0, 0] : null,
            },
            {
                text: item.date || defaultDateString,
                border: [false, false, false, isLast],
                margin: isFirst ? [0, 5, 0, 0] : null,
            },
            {
                text: `${(item.unitPrice * item.quantity).toFixed(decimalPlaces)} ${currency}`.trim(),
                border: [false, false, false, isLast],
                margin: isFirst ? [0, 5, 0, 0] : null,
                style: 'rightAlign',
            },
        ])
    })
    const totalNetPrice = totalGrossPrice * (1 + taxRate);
    const totalTax = totalGrossPrice * taxRate;
    tableDef.table.body.push([
        {
            text: '',
            border: [false, false, false, false],
            margin: [0, 5, 0, 0],
        },
        {
            text: '',
            border: [false, false, false, false],
            margin: [0, 5, 0, 0],
        },
        {
            text: 'Sub total',
            border: [false, false, false, false],
            margin: [0, 5, 0, 0],
        },
        {
            text: `${totalGrossPrice.toFixed(2)} ${currency}`.trim(),
            border: [false, false, false, false],
            margin: [0, 5, 0, 0],
            style: 'rightAlign',
        }
    ])
    tableDef.table.body.push([
        {
            text: '',
            border: [false, false, false, false],
        },
        {
            text: '',
            border: [false, false, false, false],
        },
        {
            text: `${taxType} (${taxRate * 100}%)`,
            border: [false, false, false, false],
        },
        {
            text: `${totalTax.toFixed(decimalPlaces)} ${currency}`.trim(),
            border: [false, false, false, false],
            style: 'rightAlign',
        }
    ])
    tableDef.table.body.push([
        {
            text: '',
            border: [false, false, false, false],
        },
        {
            text: '',
            border: [false, false, false, false],
        },
        {
            text: `Total`,
            border: [false, false, false, false],
            style: 'bold',
        },
        {
            text: `${totalNetPrice.toFixed(decimalPlaces)} ${currency}`.trim(),
            style: 'rightAlign',
            border: [false, false, false, false],
        }
    ])
    return tableDef;
}
